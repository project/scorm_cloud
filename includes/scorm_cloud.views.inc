<?php
/**
 * @file
 *   Implementations of Views 2 Hooks for the scorm_cloud module
 */


/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function scorm_cloud_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'scorm_cloud') . '/includes',
    ),
    'handlers' => array(
      // filter handlers
      'views_handler_field_scorm_cloud_registration_field' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugins
 */
function scorm_cloud_views_plugins() {
  return array(
    'argument validator' => array(
      'scorm_cloud_node' => array(
        'title' => t('SCORM Cloud Course Node'),
        'handler' => 'views_plugin_argument_validate_scorm_cloud_node',
        'path' => drupal_get_path('module', 'scorm_cloud') . '/includes',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data()
 */
function scorm_cloud_views_data() {

  // Basic table information.
  $data['scorm_cloud_registration'] = array(
    // Table Definition
    'table' => array(
      'group' => t('SCORM Cloud Registration'),
      // Base tables:
      'base' => array(
        'field' => 'id',
        'title' => t('SCORM Cloud Registrations'),
        'help' => t('User registrations for SCORM Cloud courses.'),
        'weight' => 0,
      ),
      'join' => array(
        'user' => array(
          'left_field'  => 'uid',
          'field'       => 'uid',
        ),
        'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',
        ),
      ),

    ),
  );

  // id
  $data['scorm_cloud_registration']['id'] = array(
    'title' => t('Registration ID'),
    'help' => t('The registration ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  // uid
  $data['scorm_cloud_registration']['uid'] = array(
    'title' => t('Registered User'),
    'help' => t('The Drupal user who registered for the course.'), // The help that appears on the UI,
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Registered User'),
    ),
  );

  // nid
  $data['scorm_cloud_registration']['nid'] = array(
    'title' => t('Registration Course Node'),
    'help' => t('The course node associated with the registration.'), // The help that appears on the UI,
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'nid',
      'label' => t('Coures Node'),
    ),
  );

  $data['scorm_cloud_registration']['course_id'] = array(
    'title' => t('Course ID'),
    'help' => t('The SCORM Cloud Course ID for this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['scorm_cloud_registration']['date_first_launched'] = array(
    'title' => t('First Launched'),
    'help' => t('The date the course was first launched by the user using this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['scorm_cloud_registration']['date_last_launched'] = array(
    'title' => t('Last Launched'),
    'help' => t('The date the course was most recently launched by the user using this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['scorm_cloud_registration']['date_completed'] = array(
    'title' => t('Date Completed'),
    'help' => t('The date the course was completed by the user using this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['scorm_cloud_registration']['complete'] = array(
    'title' => t('Status'),
    'help' => t('The completion status of this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );



  $data['scorm_cloud_registration']['success'] = array(
    'title' => t('Success'),
    'help' => t('The pass/fail status of this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['scorm_cloud_registration']['score'] = array(
    'title' => t('Final Score'),
    'help' => t('The score the user achieved on the course using this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['scorm_cloud_registration']['totaltime'] = array(
    'title' => t('Time Spent'),
    'help' => t('The time spent so far on this course.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['scorm_cloud_registration']['num_launches'] = array(
    'title' => t('Number of Launches'),
    'help' => t('The number of times the user has launched the course using this registration.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['scorm_cloud_registration']['take_number'] = array(
    'title' => t('Take Number'),
    'help' => t('The sequence of the registration when a user has taken a course multiple times.'),
    'field' => array(
      'handler' => 'views_handler_field_scorm_cloud_registration_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
