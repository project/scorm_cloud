<?php
/**
 * @file
 * Contains the 'scorm cloud node' argument validator plugin.
 */

module_load_include('inc', 'views', 'modules/node/views_plugin_argument_validate_node');


/**
 * Validate whether an argument is an acceptable SCORM Cloud node.
 */
class views_plugin_argument_validate_scorm_cloud_node extends views_plugin_argument_validate_node {
  var $option_name = 'validate_argument_node_type';

  function validate_form(&$form, &$form_state) {
    parent::validate_form($form, $form_state);
    // Do not allow the user to select which types are valid. Only SCORM Cloud course types are allowed
    unset($form[$this->option_name]);
  }

  function validate_argument($argument) {
    // Set the allowed type list to those that can be SCORM Cloud courses.
    $types = scorm_cloud_get_course_types();
    $this->argument->options[$this->option_name] = array();
    if ($types) {
      $this->argument->options[$this->option_name] = array_combine($types, $types);
    }
    return parent::validate_argument($argument);
  }
}

