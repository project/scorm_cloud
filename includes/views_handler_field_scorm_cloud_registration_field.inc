<?php

/**
 * @file
 * Contains a views field.
 */
class views_handler_field_scorm_cloud_registration_field extends views_handler_field {
  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    $value = $values->{$this->field_alias};
    return scorm_cloud_course_registration_render_field($value, $this->real_field);
  }

}