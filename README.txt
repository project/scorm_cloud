--------------------------------------------------------------------------------
SCORM Cloud for Drupal 6.x
  Developed by Ronan Dowling, Gorton Studios 
    - ronan (at) gortonstudios (dot) com
  Sponsored by Pennsylvania Rural Water Association 
    - http://www.prwa.com
--------------------------------------------------------------------------------

DESCRIPTION:
------------
This module provides Drupal integration with the SCORM Cloud API.
  http://scorm.com

INSTALLATION:
You must install the SCORM Cloud PHP library to use this module. To install the library, either:

1) Download the library from
      https://github.com/RusticiSoftware/SCORMCloud_PHPLibrary
   extract the archive and rename the resulting directory as
      scorm_cloud
   and place in 'sites/all/libraries' or 'sites/default/libraries'.

OR

2) Run the drush command
    drush scorm-cloud-dl

CONFIGURATION:
--------------
Once the PHP library is installed and the module is enabled, you will need to configure it at:
  admin/settings/scorm_cloud
You will need to enter your SCORM Cloud AppID and Secret Key available in your SCORM Cloud account at: 
  http://cloud.scorm.com/

USAGE:
------
The base SCORM Cloud module does nothing on it's own, so you will also need to install the included SCORM Cloud Field module which provides a CCK field for adding SCORM Cloud courses to a node. Once you have added a SCORM Cloud Field to a node you will be able to use to use it to upload a SCORM course file which will be sent added as a course to your account in SCORM Cloud. 

You will also need to set up the permissions to allow users to register for and take the uploaded courses. The module permissions will allow you to decide which roles can take, retake, preview and review your SCORM Cloud courses.

OPTIONAL:
---------
If you have views enabled you will be able to create views of the course registrations created by your users. An example view is provided called
  scorm_cloud_registrations_admin

QUICK START:
------------
1. Install / enable SCORM Cloud module as explained above.
2. Create SCORM Cloud account at http://cloud.scorm.com.	 
3. Enter SCORM Cloud AppID and Secret Key here admin/settings/scorm_cloud.
	Multiple AppIDs can be created within your SCORM Cloud account.
	Only 1 AppID can be referenced by the Drupal SCORM Cloud module.
	Change the AppID referenced in Drupal for testing or a fresh start.
4. Define SCORM Cloud course node type(s) and add SCORM Cloud Course field.
5. Define user roles for your LMS such as:
	DrupalAdmin - access all admin functions, reports, and SCORM Cloud config.
	LMS_Admin - access some admin functions and admin/reports/scorm_cloud.
	LMS_TrainerPrivileged - create SCORM Cloud courses and view all registrations.
	LMS_Trainer - create SCORM Cloud courses and view registrations for own courses.
	LMS_Student - take SCORM Cloud courses and view own registrations.
	authenticated - optionally use authenticated user for student role.
6. Optionally sell access to SCORM Cloud courses with additional modules
	ubercart
	uc_node_access
	content_access
7. Optionally use Views and additional modules to list courses and current user results
	views
	views_ignore_node_permissions
8. Optionally override SCORM course player CSS in SCORM Course field CCK definition
	Copy baseline CSS here - http://cloud.scorm.com/sc/css/cloudPlayer/cloudstyles.css
	Post alternate CSS here - http://yourdomain.com/alternate.css

SUPPORT:
--------	
Determine if your question is module related or SCORM Cloud related.
Drupal module support here - http://drupal.org/project/issues/scorm_cloud
SCORM Cloud and SCORM Cloud PHP Library support here - http://support.scorm.com/home

FUTURE IDEAS:
-------------
1. Expose more course property settings from http://cloud.scorm.com to the module.
2. Sell course re-takes
3. Revoke access to course when completed X times, but always allow view results
4. Certificates of completion 
5. Prerequisites courses completed to take a particular course 
6. Integrate other course formats into our Views course list and result data display
	http://drupal.org/project/cck_scorm - a free SCORM 1.2 player
	http://drupal.org/project/quiz
