<?php

/**
 * @file
 * Contains functions handling views integration.
 */

/**
 * Implementation of hook_views_handlers().
 */
function scorm_cloud_field_views_handlers() {
}

/**
 * Return CCK Views data for the scorm_cloud_field_field_settings($op == 'views data').
 */
function scorm_cloud_field_views_content_field_data($field) {
  // Build the automatic views data provided for us by CCK.
  // This creates all the information necessary for the "url" field.
  $data = content_views_field_views_data($field);

  $db_info = content_database_info($field);
  $table_alias = content_views_tablename($field);
  $field_types = _content_field_types();

  // Tweak the automatic views data for the scorm_cloud_field "url" field.
  // Set the filter title to "@label Course ID"
  $data[$table_alias][$field['field_name'] .'_course_id']['filter']['title'] = t('@label Course ID', array('@label' => t($field_types[$field['type']]['label']))) .': '. t($field['widget']['label']);

  // Build out additional views data for the scorm_cloud_field "title" field.
  $data[$table_alias][$field['field_name'] .'_title'] = array(
    'group' => t('Content'),
    'title' => t('@label title', array('@label' => t($field_types[$field['type']]['label']))) .': '. t($field['widget']['label']) .' ('. $field['field_name'] .')',
    'help' =>  $data[$table_alias][$field['field_name'] .'_course_id']['help'],
    'argument' => array(
      'field' => $db_info['columns']['title']['column'],
      'tablename' => $db_info['table'],
      'handler' => 'content_handler_argument_string',
      'click sortable' => TRUE,
      'name field' => '', // TODO, mimic content.views.inc :)
      'content_field_name' => $field['field_name'],
      'allow_empty' => TRUE,
    ),
    'filter' => array(
      'field' => $db_info['columns']['title']['column'],
      'title' => t('@label title', array('@label' => t($field_types[$field['type']]['label']))) .': '. t($field['widget']['label']),
      'tablename' => $db_info['table'],
      'handler' => 'content_handler_filter_string',
      'additional fields' => array(),
      'content_field_name' => $field['field_name'],
      'allow_empty' => TRUE,
    ),
    'sort' => array(
      'field' => $db_info['columns']['title']['column'],
      'tablename' => $db_info['table'],
      'handler' => 'content_handler_sort',
      'content_field_name' => $field['field_name'],
      'allow_empty' => TRUE,
    ),
  );

  $data[$table_alias]['table']['join']['scorm_cloud_registration'] = array(
    'table' => $db_info['table'],
    'left_field' => 'course_id',
    'field' => $field['field_name'] .'_course_id',
  );
  $data[$table_alias]['vid'] = array(
    'title' => t($field['widget']['label']),
    'help' => t('The node that the registered course is attached to'),
    'relationship' => array(
      'label' => t($field['widget']['label']),
      'base' => 'node',
      'base field' => 'vid',
      // This allows us to not show this relationship if the base is already
      // node so users won't create circular relationships.
      'skip base' => array('node', 'node_revisions'),
    ),
  );

  return $data;
}
